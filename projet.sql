-- BASH/> set NLS_LANG=.AL32UTF8

SET SERVEROUTPUT ON

DROP TABLE Move;
DROP TABLE MoveType;
DROP TABLE Piece;
DROP TABLE PieceType;
DROP TABLE PlayGame;
DROP TABLE Game;
DROP TABLE Player;
DROP TABLE Color;

CREATE TABLE Color (
    id      INTEGER     CONSTRAINT pk_Color  PRIMARY KEY,
    name VARCHAR2(5)
);

CREATE TABLE Player (
    id      INTEGER     CONSTRAINT pk_Player PRIMARY KEY,
    name    VARCHAR2(40),
    elo     FLOAT(24)
);

CREATE TABLE Game (
    id      INTEGER     CONSTRAINT pk_Game   PRIMARY KEY,
    winner  INTEGER     CONSTRAINT fk_Game_Player REFERENCES Player(id),
    dateC   DATE        DEFAULT SYSDATE
);

CREATE TABLE PlayGame (
    player  INTEGER     CONSTRAINT fk_PlayGame_Player REFERENCES Player(id),
    game    INTEGER     CONSTRAINT fk_PlayGame_Game     REFERENCES Game(id),
    color   INTEGER     CONSTRAINT fk_PlayGame_Color   REFERENCES Color(id),
                        CONSTRAINT key_PlayGame_player_game     UNIQUE (player, game),
                        CONSTRAINT key_PlayGame_game_color      UNIQUE (game, color)
);

CREATE TABLE PieceType (
    typeP      INTEGER,
    color INTEGER CONSTRAINT fk_PieceType_color_Color_id REFERENCES Color(id),
    name VARCHAR2(10),
    displayChar NCHAR(1),
    CONSTRAINT pk_PieceType PRIMARY KEY (typeP, color)
);

CREATE TABLE Piece (
    id      INTEGER     CONSTRAINT pk_Piece PRIMARY KEY,
    color   INTEGER     CONSTRAINT fk_Piece_color_Color_id      REFERENCES Color(id),
    typeP   INTEGER     ,
    game    INTEGER     CONSTRAINT fk_Piece_Game                REFERENCES Game(id),
    CONSTRAINT fk_Piece_PieceType_Color                         FOREIGN KEY (typeP, color) REFERENCES PieceType(typeP, color)
);

CREATE TABLE MoveType (
    id      INTEGER     CONSTRAINT pk_MoveType PRIMARY KEY,
    name    VARCHAR2(30)
);

CREATE TABLE Move (
    id      INTEGER     CONSTRAINT pk_Move PRIMARY KEY,
    round   INTEGER,
    piece   INTEGER     CONSTRAINT fk_Move_Piece       REFERENCES Piece(id),
    typeM   INTEGER     CONSTRAINT fk_Move_MoveType     REFERENCES MoveType(id),
    posX    CHAR(1)     CONSTRAINT check_posX CHECK(posX IN('A','B','C','D','E','F','G','H')),
    posY    CHAR(1)     CONSTRAINT check_posY CHECK(posY IN('1','2','3','4','5','6','7','8'))
);

INSERT INTO MoveType VALUES (1, 'spawned at');
INSERT INTO MoveType VALUES (2, 'moved to');
INSERT INTO MoveType VALUES (3, 'was captured at');
INSERT INTO MoveType VALUES (4, 'captured piece at');
INSERT INTO MoveType VALUES (5, 'has castled at');

INSERT INTO Color VALUES (1, 'White');
INSERT INTO Color VALUES (2, 'Black');

INSERT INTO PieceType VALUES (1, 1, 'King',     UNISTR('\2654'));
INSERT INTO PieceType VALUES (2, 1, 'Queen',    UNISTR('\2655'));
INSERT INTO PieceType VALUES (3, 1, 'Rook',     UNISTR('\2656'));
INSERT INTO PieceType VALUES (4, 1, 'Bishop',   UNISTR('\2657'));
INSERT INTO PieceType VALUES (5, 1, 'Knight',   UNISTR('\2658'));
INSERT INTO PieceType VALUES (6, 1, 'Pawn',     UNISTR('\2659'));

INSERT INTO PieceType VALUES (1, 2, 'King',     UNISTR('\265A'));
INSERT INTO PieceType VALUES (2, 2, 'Queen',    UNISTR('\265B'));
INSERT INTO PieceType VALUES (3, 2, 'Rook',     UNISTR('\265C'));
INSERT INTO PieceType VALUES (4, 2, 'Bishop',   UNISTR('\265D'));
INSERT INTO PieceType VALUES (5, 2, 'Knight',   UNISTR('\265E'));
INSERT INTO PieceType VALUES (6, 2, 'Pawn',     UNISTR('\265F'));


CREATE OR REPLACE TRIGGER t_Game_beforeInsert
BEFORE INSERT ON Game
FOR EACH ROW
DECLARE
    v_id Game.id%TYPE;
BEGIN
    SELECT max(Game.id)+1 INTO v_id FROM Game;
    IF v_id IS NULL THEN v_id := 0; END IF;
    :NEW.id := v_id;
END;
/

CREATE OR REPLACE TRIGGER t_Piece_beforeInsert
BEFORE INSERT ON Piece
FOR EACH ROW
DECLARE
    v_id Piece.id%TYPE;
BEGIN
    SELECT max(Piece.id)+1 INTO v_id FROM Piece;
    IF v_id IS NULL THEN v_id := 0; END IF;
    :NEW.id := v_id;
END;
/

CREATE OR REPLACE TRIGGER t_Move_beforeInsert
BEFORE INSERT ON Move
FOR EACH ROW
DECLARE
    v_id Move.id%TYPE;
BEGIN
    SELECT max(Move.id)+1 INTO v_id FROM Move;
    IF v_id IS NULL THEN v_id := 0; END IF;
    :NEW.id := v_id;
END;
/

CREATE OR REPLACE TRIGGER t_Player_beforeInsert
BEFORE INSERT ON Player
FOR EACH ROW
DECLARE
    v_id Player.id%TYPE;
BEGIN
    SELECT max(Player.id)+1 INTO v_id FROM Player;
    IF v_id IS NULL THEN v_id := 0; END IF;
    :NEW.id := v_id;
END;
/

--Procedures

CREATE OR REPLACE PROCEDURE initGame(p1 Player.id%TYPE, p2 Player.id%TYPE)
AS
    v_idGame Game.id%TYPE;
    v_idPiece Piece.id%TYPE;
BEGIN
    INSERT INTO Game VALUES (NULL, NULL, SYSDATE);
    SELECT max(Game.id) INTO v_idGame FROM Game;
    
    INSERT INTO PlayGame VALUES (p1, v_idGame, 1); -- White player
    INSERT INTO PlayGame VALUES (p2, v_idGame, 2); -- Black player
    
    --Pieces Blanches

    INSERT INTO Piece (color, typeP, game) VALUES (1, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0,  v_idPiece, 1, '2', 'A');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0,  v_idPiece, 1, '2', 'B');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0,  v_idPiece, 1, '2', 'C');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0,  v_idPiece, 1, '2', 'D');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0,  v_idPiece, 1, '2', 'E');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0,  v_idPiece, 1, '2', 'F');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0,  v_idPiece, 1, '2', 'G');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0,  v_idPiece, 1, '2', 'H');
    
    INSERT INTO Piece (color, typeP, game) VALUES (1, 3, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0,  v_idPiece, 1, '1', 'A');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 5, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '1', 'B');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 4, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '1', 'C');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 1, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '1', 'E');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 2, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '1', 'D');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 4, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '1', 'F');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 5, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '1', 'G');
    INSERT INTO Piece (color, typeP, game) VALUES (1, 3, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '1', 'H');

    --Pieces Noires

    INSERT INTO Piece (color, typeP, game) VALUES (2, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '7', 'A');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '7', 'B');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '7', 'C');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '7', 'D');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '7', 'E');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '7', 'F');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '7', 'G');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 6, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '7', 'H');

    INSERT INTO Piece (color, typeP, game) VALUES (2, 3, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '8', 'A');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 5, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '8', 'B');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 4, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '8', 'C');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 1, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '8', 'E');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 2, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '8', 'D');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 4, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '8', 'F');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 5, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '8', 'G');
    INSERT INTO Piece (color, typeP, game) VALUES (2, 3, v_idGame) RETURNING id INTO v_idPiece;
    INSERT INTO Move (round, piece, typeM, posY, posX) VALUES (0, v_idPiece, 1, '8', 'H');

    DBMS_OUTPUT.PUT_LINE('New game initialized with ID: ' || v_idGame);
END;
/

-- TO THE MAD MEN WHO WROTE THIS : ALWAYS LINK INTERNAL ROWS INTERATIONS TO EXTERNAL ROWS ITERATIONS.
-- AND STOP ABUSING COFFEE.
CREATE OR REPLACE VIEW ChessBoard AS SELECT (
UNISTR('\000A\001B[39;49m .  A B C D E F G H\000A 8 ')||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '8' AND posX = 'A' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '8' AND posX = 'B' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '8' AND posX = 'C' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '8' AND posX = 'D' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '8' AND posX = 'E' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '8' AND posX = 'F' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '8' AND posX = 'G' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '8' AND posX = 'H' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[39;49m\000A 7 ')||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '7' AND posX = 'A' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '7' AND posX = 'B' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '7' AND posX = 'C' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '7' AND posX = 'D' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '7' AND posX = 'E' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '7' AND posX = 'F' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '7' AND posX = 'G' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '7' AND posX = 'H' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[39;49m\000A 6 ')||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '6' AND posX = 'A' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '6' AND posX = 'B' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '6' AND posX = 'C' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '6' AND posX = 'D' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '6' AND posX = 'E' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '6' AND posX = 'F' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '6' AND posX = 'G' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '6' AND posX = 'H' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[39;49m\000A 5 ')||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '5' AND posX = 'A' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '5' AND posX = 'B' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '5' AND posX = 'C' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '5' AND posX = 'D' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '5' AND posX = 'E' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '5' AND posX = 'F' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '5' AND posX = 'G' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '5' AND posX = 'H' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[39;49m\000A 4 ')||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '4' AND posX = 'A' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '4' AND posX = 'B' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '4' AND posX = 'C' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '4' AND posX = 'D' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '4' AND posX = 'E' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '4' AND posX = 'F' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '4' AND posX = 'G' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '4' AND posX = 'H' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[39;49m\000A 3 ')||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '3' AND posX = 'A' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '3' AND posX = 'B' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '3' AND posX = 'C' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '3' AND posX = 'D' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '3' AND posX = 'E' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '3' AND posX = 'F' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '3' AND posX = 'G' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '3' AND posX = 'H' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[39;49m\000A 2 ')||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '2' AND posX = 'A' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '2' AND posX = 'B' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '2' AND posX = 'C' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '2' AND posX = 'D' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '2' AND posX = 'E' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '2' AND posX = 'F' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '2' AND posX = 'G' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '2' AND posX = 'H' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[39;49m\000A 1 ')||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '1' AND posX = 'A' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '1' AND posX = 'B' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '1' AND posX = 'C' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '1' AND posX = 'D' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '1' AND posX = 'E' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '1' AND posX = 'F' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[37;40m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '1' AND posX = 'G' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[30;47m ')||COALESCE((SELECT displayChar FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) WHERE posY = '1' AND posX = 'H' AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = g.id ) AND typeM != 3),UNISTR('.'))||
UNISTR('\001B[39;49m\000A')
) board, g.id AS gameId FROM Game g;

CREATE OR REPLACE FUNCTION getIdPiece(idGame Game.id%TYPE, x Move.posX%TYPE, y Move.posY%TYPE)
RETURN Piece.id%TYPE
AS
    v_res Piece.id%TYPE;
BEGIN
    SELECT piece INTO v_res FROM Move cMove WHERE posX = x AND posY = y AND round =
    (   SELECT MAX(Move.round)
        FROM Move JOIN Piece ON (Piece.id = Move.piece)
        WHERE Move.piece = cMove.piece AND Piece.game = idGame  )
    AND typeM != 3;
    
    RETURN v_res;
END;
/

CREATE OR REPLACE PROCEDURE movePiece(idG Game.id%TYPE, x1 Move.posX%TYPE, y1 Move.posY%TYPE, x2 Move.posX%TYPE, y2 Move.posY%TYPE)
AS
    v_idPiece Piece.id%TYPE;
    v_typePiece Piece.typeP%TYPE;
    v_round Move.round%TYPE;
BEGIN
    SELECT MAX(round)+1 INTO v_round FROM Move JOIN Piece ON Move.piece = Piece.id WHERE Piece.game = idG;
    v_idPiece := getIdPiece(idG, x1, y1);
    SELECT typeP INTO v_typePiece FROM Piece WHERE id = v_idPiece;
    
    INSERT INTO Move (round, piece, typeM, posX, posY) VALUES (v_round, v_idPiece, 2, x2, y2);
END;
/

CREATE OR REPLACE PROCEDURE capturePiece(idG Game.id%TYPE, x1 Move.posX%TYPE, y1 Move.posY%TYPE, x2 Move.posX%TYPE, y2 Move.posY%TYPE)
AS
    v_idPiece Piece.id%TYPE;
    v_typePiece Piece.typeP%TYPE;
    v_round Move.round%TYPE;
BEGIN
    SELECT MAX(round)+1 INTO v_round FROM Move JOIN Piece ON Move.piece = Piece.id WHERE Piece.game = idG;
    v_idPiece := getIdPiece(idG, x1, y1);
    SELECT typeP INTO v_typePiece FROM Piece WHERE id = v_idPiece;
    
    INSERT INTO Move (round, piece, typeM, posX, posY) VALUES (v_round, v_idPiece, 4, x2, y2);
END;
/

CREATE OR REPLACE PROCEDURE castling(gameId Game.id%TYPE, xKing Move.posX%TYPE, yKing Move.posY%TYPE, xRook Move.posX%TYPE, yRook Move.posY%TYPE)
AS
    v_canCastling BOOLEAN;
    v_round Move.round%TYPE;
    v_kingId Piece.id%TYPE;
    v_kingType Piece.typeP%TYPE;
    v_rookId Piece.id%TYPE;
    v_rookType Piece.typeP%TYPE;
BEGIN
    v_kingId := getIdPiece(gameId, xKing, yKing);
    v_rookId := getIdPiece(gameId, xRook, yRook);
    v_canCastling := false;
    
    SELECT MAX(round)+1 INTO v_round FROM Move JOIN Piece ON Move.piece = Piece.id WHERE Piece.game = gameId;
    SELECT typeP INTO v_kingType FROM Piece WHERE id = v_kingId;
    SELECT typeP INTO v_rookType FROM Piece WHERE id = v_rookId;
    
    IF v_kingType = 1 AND v_rookType = 3 THEN
        IF moveCount(v_kingId) = 0 AND moveCount(v_rookId) = 0 THEN 
            IF coord2number(xRook) - coord2number(xKing) = 3 THEN
                IF isPathFree(gameId, coord2number(xKing), coord2number(yKing), coord2number(xRook), coord2number(yRook)) THEN
                    v_canCastling := true;
                END IF;
            END IF;
        END IF;
    END IF;
    
    IF v_canCastling = false THEN
        RAISE_APPLICATION_ERROR(-20004, 'Erreur : le roque n''est pas possible dans cette situation !');
    ELSE
        IF coord2number(xRook) - coord2number(xKing) = 3 THEN
            INSERT INTO Move (round, piece, typeM, posX, posY) VALUES (v_round, v_kingId, 5, number2col(coord2number(xRook)-1), yKing);
            INSERT INTO Move (round, piece, typeM, posX, posY) VALUES (v_round, v_rookId, 5, number2col(coord2number(xKing)+1), yRook);
        ELSE
            INSERT INTO Move (round, piece, typeM, posX, posY) VALUES (v_round, v_kingId, 5, number2col(coord2number(xRook)+2), yKing);
            INSERT INTO Move (round, piece, typeM, posX, posY) VALUES (v_round, v_rookId, 5, number2col(coord2number(xKing)-1), yRook);
        END IF;
    END IF;
END;
/


CREATE OR REPLACE FUNCTION coord2number(coord CHAR)
RETURN INTEGER
AS
    v_res INTEGER;
BEGIN
    CASE coord
        WHEN 'A' THEN v_res := 0;
        WHEN 'B' THEN v_res := 1;
        WHEN 'C' THEN v_res := 2;
        WHEN 'D' THEN v_res := 3;
        WHEN 'E' THEN v_res := 4;
        WHEN 'F' THEN v_res := 5;
        WHEN 'G' THEN v_res := 6;
        WHEN 'H' THEN v_res := 7;
        WHEN '1' THEN v_res := 0;
        WHEN '2' THEN v_res := 1;
        WHEN '3' THEN v_res := 2;
        WHEN '4' THEN v_res := 3;
        WHEN '5' THEN v_res := 4;
        WHEN '6' THEN v_res := 5;
        WHEN '7' THEN v_res := 6;
        WHEN '8' THEN v_res := 7;
        ELSE v_res := -1;
    END CASE;
    
    RETURN v_res;
END;
/

CREATE OR REPLACE FUNCTION moveCount(idPiece Piece.id%TYPE)
RETURN INTEGER
AS
    v_res INTEGER;
BEGIN
    v_res := 0;
    
    SELECT COUNT(*) INTO v_res FROM Move WHERE piece = idPiece AND typeM != 1;
    
    RETURN v_res;
END;
/

CREATE OR REPLACE FUNCTION isOccupiedByPiece(idGame Game.id%TYPE, p_posX Move.posX%TYPE, p_posY Move.posY%TYPE)
RETURN BOOLEAN
AS
    v_id Piece.id%TYPE;
BEGIN
    SELECT Piece.id INTO v_id FROM Move cMove JOIN Piece ON (cMove.piece = Piece.id) WHERE posX = p_posX AND posY = p_posY AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = idGame ) AND typeM != 3;
    RETURN true;
    
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN false;
END;
/

CREATE OR REPLACE FUNCTION number2col(col INTEGER)
RETURN CHAR
AS
    v_res CHAR;
BEGIN
    CASE col
        WHEN 0 THEN
            v_res := 'A';
        WHEN 1 THEN
            v_res := 'B';
        WHEN 2 THEN
            v_res := 'C';
        WHEN 3 THEN
            v_res := 'D';
        WHEN 4 THEN
            v_res := 'E';
        WHEN 5 THEN
            v_res := 'F';
        WHEN 6 THEN
            v_res := 'G';
        WHEN 7 THEN
            v_res := 'H';
        ELSE
            RAISE_APPLICATION_ERROR(-20003, 'Erreur : caractère non supporté !');
    END CASE;
    
    RETURN v_res;
END;
/

CREATE OR REPLACE FUNCTION number2lig(lig INTEGER)
RETURN CHAR
AS
    v_res CHAR;
BEGIN
    CASE lig
        WHEN 0 THEN
            v_res := '1';
        WHEN 1 THEN
            v_res := '2';
        WHEN 2 THEN
            v_res := '3';
        WHEN 3 THEN
            v_res := '4';
        WHEN 4 THEN
            v_res := '5';
        WHEN 5 THEN
            v_res := '6';
        WHEN 6 THEN
            v_res := '7';
        WHEN 7 THEN
            v_res := '8';
        ELSE
            RAISE_APPLICATION_ERROR(-20003, 'Erreur : caractère non supporté !');
    END CASE;
    
    RETURN v_res;
END;
/

CREATE OR REPLACE FUNCTION isPathFree(gameId Game.id%TYPE, x1 INTEGER, y1 INTEGER, x2 INTEGER, y2 INTEGER, f BOOLEAN := true)
RETURN BOOLEAN
AS
    v_tmpX2 INTEGER;
    v_tmpY2 INTEGER;
BEGIN
    v_tmpX2 := x2;
    v_tmpY2 := y2;
    
    IF x1 = x2 AND y1 = y2 THEN
        RETURN true;
    ELSE
        IF isOccupiedByPiece(gameId, number2col(v_tmpX2), number2lig(v_tmpY2)) = true AND f = false THEN
            RETURN false;
        ELSE
            IF x1 > x2 THEN
                v_tmpX2 := v_tmpX2 + 1;
            ELSIF x1 < x2 THEN
                v_tmpX2 := v_tmpX2 - 1;
            END IF;
            
            IF y1 > y2 THEN
                v_tmpY2 := v_tmpY2 + 1;
            ELSIF y1 < y2 THEN
                v_tmpY2 := v_tmpY2 - 1;
            END IF;
        RETURN isPathFree(gameId, x1 ,y1, v_tmpX2, v_tmpY2, false);
        END IF;
    END IF;
END;
/ 

CREATE OR REPLACE TRIGGER t_onNewPlayer
BEFORE INSERT ON Player
FOR EACH ROW
BEGIN
    :NEW.elo := 1000;
END;
/

--Triggers des déplacements
CREATE OR REPLACE TRIGGER t_checkMove
BEFORE INSERT ON Move
FOR EACH ROW
WHEN (NEW.typeM = 2)
DECLARE
    v_isMoveValid BOOLEAN;
    v_typeP Piece.typeP%TYPE;
    v_color Piece.color%TYPE;
    v_idGame Game.id%TYPE;
    v_oldX Move.posY%TYPE;
    v_oldY Move.posX%TYPE;
BEGIN
    v_isMoveValid := true;

    SELECT typeP INTO v_typeP FROM Piece WHERE id = :NEW.piece;
    SELECT color INTO v_color FROM Piece WHERE id = :NEW.piece;
    SELECT game INTO v_idGame FROM Piece WHERE id = :NEW.piece;
    SELECT posX INTO v_oldX FROM Move cMove WHERE piece = :NEW.piece AND typeM != 3 AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = v_idGame);    
    SELECT posY INTO v_oldY FROM Move cMove WHERE piece = :NEW.piece AND typeM != 3 AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = v_idGame);
    
    
    IF isOccupiedByPiece(v_idGame, :NEW.posX, :NEW.posY) = true THEN
        v_isMoveValid := false;
    ELSE
        CASE v_typeP
            WHEN 6 THEN
            BEGIN
                --Pawn
                IF v_oldX != :NEW.posX THEN
                    v_isMoveValid := false;
                ELSE
                    IF moveCount(:NEW.piece) = 0 THEN
                        IF v_color = 1 THEN
                            IF coord2number(:NEW.posY) - coord2number(v_oldY) > 2 OR coord2number(:NEW.posY) - coord2number(v_oldY) < 1 THEN
                                v_isMoveValid := false;
                            END IF;
                        ELSE
                            IF coord2number(:NEW.posY) - coord2number(v_oldY) < -2 OR coord2number(:NEW.posY) - coord2number(v_oldY) > -1 THEN
                                v_isMoveValid := false;
                            END IF;
                        END IF;
                    ELSE
                        IF v_color = 1 THEN
                            IF coord2number(:NEW.posY) - coord2number(v_oldY) > 1 OR coord2number(:NEW.posY) - coord2number(v_oldY) < 1 THEN
                                v_isMoveValid := false;
                            END IF;
                        ELSE
                            IF coord2number(:NEW.posY) - coord2number(v_oldY) < -1 OR coord2number(:NEW.posY) - coord2number(v_oldY) > -1 THEN
                                v_isMoveValid := false;
                            END IF;
                        END IF;
                    END IF;
                END IF;
            END;
            WHEN 5 THEN
                --Knight
                IF ((ABS(coord2number(:NEW.posX) - coord2number(v_oldX)) != 2) OR (ABS(coord2number(:NEW.posY) - coord2number(v_oldY)) != 1)) AND ((ABS(coord2number(:NEW.posX) - coord2number(v_oldX)) != 1) OR (ABS(coord2number(:NEW.posY) - coord2number(v_oldY)) != 2)) THEN
                    v_isMoveValid := false;
                END IF;
            WHEN 1 THEN
                --King
                IF (ABS(coord2number(:NEW.posX) - coord2number(v_oldX)) != 1) AND (ABS(coord2number(:NEW.posY) - coord2number(v_oldY)) != 1) THEN
                    v_isMoveValid := false;
                END IF;
            WHEN 3 THEN
                --Rook
                IF (:NEW.posX != v_oldX AND :NEW.posY != v_oldY) OR isPathFree(v_idGame, coord2number(v_oldX), coord2number(v_oldY), coord2number(:NEW.posX), coord2number(:NEW.posY)) = false THEN
                    v_isMoveValid := false;
                END IF;
            WHEN 4 THEN
                --Bishop
                IF :NEW.posX = v_oldX OR :NEW.posY = v_oldY OR isPathFree(v_idGame, coord2number(v_oldX), coord2number(v_oldY), coord2number(:NEW.posX), coord2number(:NEW.posY)) = false THEN
                    v_isMoveValid := false;
                END IF;
            WHEN 2 THEN
                IF isPathFree(v_idGame, coord2number(v_oldX), coord2number(v_oldY), coord2number(:NEW.posX), coord2number(:NEW.posY)) = false THEN
                    v_isMoveValid := false;
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20001, 'Ce type de pièce n''est pas géré !');
        END CASE;
    END IF;
    
    IF v_isMoveValid = false THEN
        RAISE_APPLICATION_ERROR(-20002, 'Erreur : mouvement invalide !');
    END IF;
END;
/

CREATE OR REPLACE TRIGGER t_checkCapture
BEFORE INSERT ON Move
FOR EACH ROW
WHEN (NEW.typeM = 4)
DECLARE
    v_isMoveValid BOOLEAN;
    v_typeP Piece.typeP%TYPE;
    v_color Piece.color%TYPE;
    v_idGame Game.id%TYPE;
    v_oldX Move.posY%TYPE;
    v_oldY Move.posX%TYPE;
    v_capturedPiece Piece.id%TYPE;
BEGIN
    v_isMoveValid := true;

    SELECT typeP INTO v_typeP FROM Piece WHERE id = :NEW.piece;
    SELECT color INTO v_color FROM Piece WHERE id = :NEW.piece;
    SELECT game INTO v_idGame FROM Piece WHERE id = :NEW.piece;
    SELECT posX INTO v_oldX FROM Move cMove WHERE piece = :NEW.piece AND typeM != 3 AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = v_idGame);    
    SELECT posY INTO v_oldY FROM Move cMove WHERE piece = :NEW.piece AND typeM != 3 AND round = (SELECT MAX(Move.round) FROM Move JOIN Piece ON (Piece.id = Move.piece) WHERE Move.piece = cMove.piece AND Piece.game = v_idGame);
    v_capturedPiece := getIdPiece(v_idGame, :NEW.posX, :NEW.posY);
    
    IF isOccupiedByPiece(v_idGame, :NEW.posX, :NEW.posY) = false THEN
        v_isMoveValid := false;
    ELSE
        CASE v_typeP
            WHEN 6 THEN
            BEGIN
                --Pawn
                NULL;
            END;
            WHEN 5 THEN
                --Knight
                IF ((ABS(coord2number(:NEW.posX) - coord2number(v_oldX)) != 2) OR (ABS(coord2number(:NEW.posY) - coord2number(v_oldY)) != 1)) AND ((ABS(coord2number(:NEW.posX) - coord2number(v_oldX)) != 1) OR (ABS(coord2number(:NEW.posY) - coord2number(v_oldY)) != 2)) THEN
                    v_isMoveValid := false;
                END IF;
            WHEN 1 THEN
                --King
                IF (ABS(coord2number(:NEW.posX) - coord2number(v_oldX)) != 1) AND (ABS(coord2number(:NEW.posY) - coord2number(v_oldY)) != 1) THEN
                    v_isMoveValid := false;
                END IF;
            WHEN 3 THEN
                --Rook
                IF (:NEW.posX != v_oldX AND :NEW.posY != v_oldY) OR isPathFree(v_idGame, coord2number(v_oldX), coord2number(v_oldY), coord2number(:NEW.posX), coord2number(:NEW.posY)) = false THEN
                    v_isMoveValid := false;
                END IF;
            WHEN 4 THEN
                --Bishop
                IF :NEW.posX = v_oldX OR :NEW.posY = v_oldY OR isPathFree(v_idGame, coord2number(v_oldX), coord2number(v_oldY), coord2number(:NEW.posX), coord2number(:NEW.posY)) = false THEN
                    v_isMoveValid := false;
                END IF;
            WHEN 2 THEN
                IF isPathFree(v_idGame, coord2number(v_oldX), coord2number(v_oldY), coord2number(:NEW.posX), coord2number(:NEW.posY)) = false THEN
                    v_isMoveValid := false;
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20001, 'Ce type de pièce n''est pas géré !');
        END CASE;
    END IF;
    
    IF v_isMoveValid = false THEN
        RAISE_APPLICATION_ERROR(-20004, 'Erreur : capture invalide !');
    ELSE
        INSERT INTO Move (round, piece, typeM, posX, posY) VALUES (:NEW.round, v_capturedPiece, 3, :NEW.posX, :NEW.posY);
    END IF;
END;
/

CREATE OR REPLACE TRIGGER t_checkOnCapture
BEFORE INSERT ON Move
FOR EACH ROW
WHEN (NEW.typeM = 3)
DECLARE
    v_typeP Piece.typeP%TYPE;
    v_colorP Piece.color%TYPE;
    v_gameId Game.id%TYPE;
    v_idW   Player.id%TYPE;
    v_eloW  Player.elo%TYPE;
    v_coefW Player.elo%TYPE;
    v_nbPlayW INTEGER;
    v_idB   Player.id%TYPE; 
    v_eloB  Player.elo%TYPE;
    v_coefB Player.elo%TYPE;
    v_nbPlayB INTEGER;
    v_pDW Player.elo%TYPE;
    v_pDB Player.elo%TYPE;
BEGIN
    SELECT typeP INTO v_typeP FROM Move JOIN Piece ON (Move.piece = Piece.id) WHERE Move.piece = :NEW.piece AND posX = :NEW.posX AND posY = :NEW.posY;
    SELECT color INTO v_colorP FROM Move JOIN Piece ON (Move.piece = Piece.id) WHERE Move.piece = :NEW.piece AND posX = :NEW.posX AND posY = :NEW.posY;
    SELECT game INTO v_gameId FROM Move JOIN Piece ON (Move.piece = Piece.id) WHERE Move.piece = :NEW.piece AND posX = :NEW.posX AND posY = :NEW.posY;
    SELECT player INTO v_idW FROM PlayGame WHERE game = v_gameId AND color = 1;
    SELECT player INTO v_idB FROM PlayGame WHERE game = v_gameId AND color = 2;
    SELECT elo INTO v_eloW FROM Player WHERE id = v_idW;
    SELECT elo INTO v_eloB FROM Player WHERE id = v_idB;
    SELECT COUNT(*) INTO v_nbPlayW FROM PlayGame WHERE player = v_idW;
    SELECT COUNT(*) INTO v_nbPlayB FROM PlayGame WHERE player = v_idB;
    
    v_pDW := 1 / (1 + POWER(10, v_eloW - v_eloB));
    v_pDB := 1 / (1 + POWER(10, v_eloB - v_eloW));
    
    --Calcul coefficient ELO joueur Blanc :
    IF v_nbPlayW <= 30 THEN
        v_coefW := 40;
    ELSIF v_eloW <= 2400 THEN
        v_coefW := 20;
    ELSE
        v_coefW := 10;
    END IF;
        
    --Calcul coefficient ELO joueur Noir :
    IF v_nbPlayB <= 30 THEN
        v_coefB := 40;
    ELSIF v_eloB <= 2400 THEN
        v_coefB := 20;
    ELSE
        v_coefB := 10;
    END IF;
    
    --Calcul ELO en cas de capture d'un roi.
    IF v_typeP = 1 THEN
        IF v_colorP = 1 THEN
            v_eloW := v_eloW + v_coefW * (0.5 - v_pDW);
            v_eloB := v_eloB + v_coefB * (1 - v_pDB);
            
            UPDATE Game SET winner = v_idB WHERE id = v_gameId;
        ELSE
            v_eloW := v_eloW + v_coefW * (1 - v_pDW);
            v_eloB := v_eloB + v_coefB * (0.5 - v_pDB);
            
            UPDATE Game SET winner = v_idW WHERE id = v_gameId;        
        END IF;
        
        UPDATE Player SET elo = v_eloW WHERE Player.id = v_idW;
        UPDATE Player SET elo = v_eloB WHERE Player.id = v_idB;
        
        DBMS_OUTPUT.PUT_LINE('Echec et mat !');
    END IF;
END;
/

CREATE OR REPLACE TRIGGER t_gameFinished
BEFORE INSERT ON Move
FOR EACH ROW
DECLARE
    v_gameId Game.id%TYPE;
    v_gameWinner Game.winner%TYPE;
BEGIN
    SELECT game INTO v_gameId FROM Piece WHERE Piece.id = :NEW.piece;
    SELECT winner INTO v_gameWinner FROM Game WHERE id = v_gameId;
    
    IF v_gameWinner IS NOT NULL THEN
        RAISE_APPLICATION_ERROR(-20005, 'Erreur : la partie est terminée !');
    END IF;
END;
/

CREATE OR REPLACE TRIGGER t_checkPlayerOrder
BEFORE INSERT ON Move
FOR EACH ROW
WHEN (NEW.typeM != 1 AND NEW.typeM != 5 AND NEW.typeM != 3)
DECLARE
    v_gameId Game.id%TYPE;
    v_currColor Color.id%TYPE;
    v_currRound Move.round%TYPE;
    v_captureNumber INTEGER;
BEGIN
    SELECT game INTO v_gameId FROM Piece WHERE id = :NEW.piece;
    SELECT color INTO v_currColor FROM Piece WHERE id = :NEW.piece;
    SELECT MAX(round) INTO v_currRound FROM Move JOIN Piece ON (Move.piece = Piece.id) WHERE game = v_gameId AND typeM != 3;
    --SELECT MOD(COUNT(*),2) INTO v_captureNumber FROM Move JOIN Piece ON (Move.piece = Piece.id) WHERE game = v_gameId AND (typeM = 3 OR typeM = 4);
    
    IF (v_currColor = 1 AND MOD(v_currRound, 2) != 0) OR (v_currColor = 2 AND MOD(v_currRound, 2) = 0) /*AND v_captureNumber = 1*/ THEN
        RAISE_APPLICATION_ERROR(-20006, 'Erreur : ce n''est pas à vous de jouer !');
    END IF;
END;
/

CREATE OR REPLACE TRIGGER t_onCastling
BEFORE INSERT ON Move
FOR EACH ROW
WHEN (NEW.typeM = 5)
DECLARE
    v_gameId Game.id%TYPE;
    v_currColor Color.id%TYPE;
    v_currRound Move.round%TYPE;
    v_castlingNumber INTEGER;
BEGIN
    SELECT game INTO v_gameId FROM Piece WHERE id = :NEW.piece;
    SELECT color INTO v_currColor FROM Piece WHERE id = :NEW.piece;
    SELECT MAX(round) INTO v_currRound FROM Move JOIN Piece ON (Move.piece = Piece.id) WHERE game = v_gameId;
    SELECT MOD(COUNT(*),2) INTO v_castlingNumber FROM Move JOIN Piece ON (Move.piece = Piece.id) WHERE game = v_gameId AND typeM = 5;
    
    IF (v_currColor = 1 AND MOD(v_currRound, 2) != 0) OR (v_currColor = 2 AND MOD(v_currRound, 2) = 0) AND v_castlingNumber = 1 THEN
        RAISE_APPLICATION_ERROR(-20006, 'Erreur : ce n''est pas à vous de jouer !');
    END IF;
END;
/
/*
INSERT INTO Player (id, name) VALUES (0, 'Gabriel');
INSERT INTO Player (id, name) VALUES (1, 'Florian');

EXECUTE initGame(0,1);

SET PAGESIZE 0
SET LINESIZE 92

SELECT board FROM ChessBoard WHERE gameId = 0;
*/

