#!/bin/bash

if [ -z ${chessRunning+0} ]; then
	. /export/home/oracle/oraenv.sh
	export PATH=/opt/csw/bin/:$PATH
	export NLS_LANG=AMERICAN_AMERICA.UTF8

	export chessRunning=1
	"$0" | sqlplus
	export chessRunning=0
else
	sleep 1
	echo "gd543586"
	sleep 1
	echo "gd543586"
	sleep 1

	echo "SET SERVEROUTPUT ON"
	echo "HOST clear"

	sleep 1

	echo "prompt Shall we play a game?"

	sleep 1

	game=0

	while read line
	do
		if [[ "$line" =~ ^([A-H])([1-8])\>([A-H])([1-8])$ ]]; then
			echo "EXECUTE movePiece($game, '${BASH_REMATCH[1]}', '${BASH_REMATCH[2]}', '${BASH_REMATCH[3]}', '${BASH_REMATCH[4]}')" ;
			sleep 3;
			echo "SET PAGESIZE 0";
			echo "SET LINESIZE 92";
			echo "HOST clear";
			echo "SELECT board FROM chessBoard WHERE gameId = $game;";
		elif [[ "$line" =~ ^([A-H])([1-8])!([A-H])([1-8])$ ]]; then
			echo "EXECUTE capturePiece($game, '${BASH_REMATCH[1]}', '${BASH_REMATCH[2]}', '${BASH_REMATCH[3]}', '${BASH_REMATCH[4]}')" ;
			sleep 3;
			echo "SET PAGESIZE 0";
			echo "SET LINESIZE 92";
			echo "HOST clear";
			echo "SELECT board FROM chessBoard WHERE gameId = $game;";
		elif [[ "$line" =~ ^([A-H])([1-8])\<\>([A-H])([1-8])$ ]]; then
			echo "EXECUTE castling($game, '${BASH_REMATCH[1]}', '${BASH_REMATCH[2]}', '${BASH_REMATCH[3]}', '${BASH_REMATCH[4]}')" ;
			sleep 3;
			echo "SET PAGESIZE 0";
			echo "SET LINESIZE 92";
			echo "HOST clear";
			echo "SELECT board FROM chessBoard WHERE gameId = $game;";
		elif [[ "$line" =~ ^game=([0-9])+$ ]]; then
			game=${BASH_REMATCH[1]};
			echo "SET PAGESIZE 0";
			echo "SET LINESIZE 92";
			echo "HOST clear";
			echo "SELECT board FROM chessBoard WHERE gameId = $game;";
		elif [[ "$line" =~ ^showboard$ ]]; then
			echo "SET PAGESIZE 0";
			echo "SET LINESIZE 92";
			echo "HOST clear";
			echo "SELECT board FROM chessBoard WHERE gameId = $game;";
		elif [[ "$line" =~ ^addplayer\ (.+)$ ]]; then
			echo "INSERT INTO Player (name) VALUES ('${BASH_REMATCH[1]}');";
		elif [[ "$line" =~ ^players$ ]]; then
			echo "SET PAGESIZE 20";
			echo "SET LINESIZE 80";
			echo "COLUMN NAME FORMAT A40";
			echo "HOST clear";
			echo "SELECT * FROM Player ORDER BY id;";
		elif [[ "$line" =~ ^games$ ]]; then
			echo "SET PAGESIZE 20";
			echo "SET LINESIZE 80";
			echo "HOST clear";
			echo "SELECT * FROM Game ORDER BY id;";
		elif [[ "$line" =~ ^newgame\ ([0-9]+)\ ([0-9]+)$ ]]; then
			echo "EXECUTE initGame(${BASH_REMATCH[1]},${BASH_REMATCH[2]});";
		elif [[ "$line" =~ ^moves$ ]]; then
			echo "SET PAGESIZE 0";
			echo "SET LINESIZE 80";
			echo "COLUMN NAME FORMAT A40";
			echo "HOST clear";
			echo "SELECT 'The ' || Color.name || ' ' || PieceType.name || ' ' ||  MoveType.name || ' ' || Move.posX || Move.posY FROM Move JOIN MoveType ON (Move.typeM = MoveType.id) JOIN Piece ON (Move.piece = Piece.id) JOIN PieceType ON (Piece.typeP = PieceType.typeP AND Piece.color = PieceType.color) JOIN Color ON (PieceType.color = Color.id) WHERE game = $game ORDER BY round;";
		elif [[ "$line" =~ ^reset$ ]]; then
			echo "HOST clear";
			echo "START projet";
			echo "HOST clear";
		elif [[ "$line" =~ ^exit$ ]]; then
			echo "HOST clear";
			echo "exit";
			sleep 2;
			echo "^C";
		else
			#echo $line;
			echo "prompt Invalid command.";
		fi
	done < "${1:-/dev/stdin}"
fi
